import turtle
turtle.shape('turtle')
turtle.pensize(8)
turtle.shapesize(1.5)
turtle.speed (30000)
fundament=5
while fundament !=0:
    for i in range(fundament):
        turtle.color('black')
        turtle.begin_fill()
        for i in range(2):
            turtle.fd(60)
            turtle.left(90)
            turtle.fd(30)
            turtle.left(90)
        turtle.color('red')
        turtle.end_fill()
        turtle.color('black')
        turtle.fd(60)
    turtle.left(90)
    turtle.fd(30)
    turtle.left(90)
    fundament=fundament-1
    turtle.fd(fundament*60+30)
    turtle.left(180)

turtle.write('                          тетрадь черная как самир')
turtle.mainloop()
